package ru.tsc.tambovtsev.tm.repository;

import ru.tsc.tambovtsev.tm.api.repository.IOwnerRepository;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.AbstractUserModel;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IOwnerRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for (final M m : models) {
            if (userId.equals(m.getUserId())) result.add(m);
        }
        return result;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findById(userId, id) != null;
    }

    @Override
    public M findById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        for (final M m : models) {
            if (!id.equals(m.getUserId())) continue;
            if (!userId.equals(m.getUserId())) continue;
            return m;
        }
        return null;
    }

    @Override
    public int getSize(final String userId) {
        int count = 0;
        for (final M m : models) {
            if (userId.equals(m.getUserId())) count++;
        }
        return count;
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        final List<M> result = findAll(userId);
        result.sort(sort.getComparator());
        return result;
    }


}
