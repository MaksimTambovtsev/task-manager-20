package ru.tsc.tambovtsev.tm.repository;

import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) result.add(task);
        }
        return result;
    }

}
