package ru.tsc.tambovtsev.tm.model;

import java.util.UUID;

public abstract class AbstractEntity {

    private String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

}
