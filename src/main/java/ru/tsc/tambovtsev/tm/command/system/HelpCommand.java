package ru.tsc.tambovtsev.tm.command.system;

import ru.tsc.tambovtsev.tm.api.model.ICommand;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;

import java.util.Collection;

public class HelpCommand extends AbstractSystemCommand {

    public static final String NAME = "help";

    public static final String DESCRIPTION = "Display list of terminal commands.";

    public static final String ARGUMENT = "-h";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand command : commands) {
            System.out.println(command.toString());
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

}
