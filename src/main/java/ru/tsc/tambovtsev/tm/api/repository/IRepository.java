package ru.tsc.tambovtsev.tm.api.repository;

import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractEntity> {

    M add(M model);

    void clear();

    List<M> findAll();

    boolean existsById(String id);

    M findById(String id);

    int getSize();

    M remove(M model);

    M removeById(String id);

    List<M> findAll(Comparator comparator);

    List<M> findAll(Sort sort);

    void removeAll(Collection<M> collection);

}
