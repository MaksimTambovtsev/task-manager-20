package ru.tsc.tambovtsev.tm.api.service;

import ru.tsc.tambovtsev.tm.api.repository.IOwnerRepository;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IOwnerRepository<M>, IService<M> {

    @Override
    List<M> findAll(String userId, Sort sort);

}
