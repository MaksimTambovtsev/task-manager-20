package ru.tsc.tambovtsev.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}
