package ru.tsc.tambovtsev.tm.api.repository;

import ru.tsc.tambovtsev.tm.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

}