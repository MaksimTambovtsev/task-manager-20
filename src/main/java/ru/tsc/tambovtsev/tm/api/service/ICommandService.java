package ru.tsc.tambovtsev.tm.api.service;

import ru.tsc.tambovtsev.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand command);

    AbstractCommand getCommandByArgument(String argument);

    AbstractCommand getCommandByName(String name);

    Collection<AbstractCommand> getCommands();

}